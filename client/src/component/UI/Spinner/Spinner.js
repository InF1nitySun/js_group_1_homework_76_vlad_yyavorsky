import React from 'react';
import './Spinner.css';

const Spinner = () => (
    <div className="Loader">Data is empty, or page is Loading...</div>
);

export default Spinner;
