import React from 'react';
import {Button, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import './SendMessages.css';
import axios from '../../axios-api';

class SendMessages extends React.Component {
    state = {
        author: '',
        message: ''
    };

    changeDataHandler = event => {
        this.setState({[event.target.name]: event.target.value})
    };

    sendData = e => {
        e.preventDefault();
        if (this.state.author === '' || this.state.author === undefined || this.state.message === '' || this.state.message === undefined) {
            console.log("Author and message must be present in the request");

            alert("Author and message must be present in the request");

        } else {
            axios.post('/messages', this.state).then(() => {
                this.setState({author: '', message: ''});
            }).catch(error => {
                console.log(error);
            })
        }
    };

    render() {
        return (
            <Form inline>
                <FormGroup controlId="formInlineName">
                    <ControlLabel className="author" >Name</ControlLabel>
                    <FormControl className="authorInput" required type="text" placeholder="Author" name="author"
                                 onChange={this.changeDataHandler} value={this.state.author}/>
                </FormGroup>
                <FormGroup>
                    <ControlLabel className="message">Message</ControlLabel>
                    <FormControl className="messageInput" required type="text" placeholder="Enter your message"
                                 name="message" onChange={this.changeDataHandler} value={this.state.message}/>
                </FormGroup>
                <Button type="submit" onClick={this.sendData}>Send</Button>
            </Form>
        )
    }
}

export default SendMessages;