import React from 'react';
import {ListGroup, ListGroupItem} from "react-bootstrap";
import './ListOfMessages.css';
// import Spinner from "../../component/UI/Spinner/Spinner";

const ListOfMessages = props => {

    // console.log(props);// блин зря корячился, с этим спинером.. забыл про обновление сервера

    let form = (
        <div>
            {props.messages.map(message => (
                    <ListGroupItem key={message.id}>
                        <h5 className="name">{message.author}</h5>
                        <p className="date">{message.date}</p>
                        {message.message}
                    </ListGroupItem>
                )
            ).reverse()}
        </div>
    );
    // if (props.loading) {
    //     form = <Spinner/>;
    // }

    return (
        <ListGroup>
            {form}
        </ListGroup>
    )
};

export default ListOfMessages;