const express = require('express');
const router = express.Router();

const createRouter = (db) => {

    router.get('/', (req, res) => {
        if (req.query.date) {

            let date = req.query.date;
            res.send(db.getDataByDataTime(date));
        } else if (req.query.id) {

            let id = req.query.id;
            res.send(db.getDataById(id));
        } else {

            let result = db.getData().slice(-30);
            res.send(result);
        }

    });

    router.post('/', (req, res) => {
        const product = req.body;
        if (product.author === '' || product.author === undefined || product.message === '' || product.message === undefined) {

            console.log("Author and message must be present in the request");

            res.status(400).send({"error": "Author and message must be present in the request"});
        } else {
            db.addItem(product).then(result => {
                res.send(result);
            });
        }
    });

    return router;
};

module.exports = createRouter;