const fs = require('fs');
const nanoid = require("nanoid");

let data = [];
const dbFold = './messages/';
let dbFile = `${dbFold}/db.json`;


module.exports = {
    init: () => {
        return new Promise((resolve, reject) => {
            fs.readFile(dbFile, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    data = JSON.parse(result);
                    resolve(data);
                }
            });
        });
    },
    getData: () => data,
    getDataById: id => {
        return data.find(item => item.id === id);
    },
    getDataByDataTime: date => {
        return data.find(item => item.date === date);
    },
    addItem: (item) => {
        item.id = nanoid();
        item.date = new Date().toISOString();


        data.push(item);

        let contents = JSON.stringify(data, null, 2);

        return new Promise((resolve, reject) => {
            fs.writeFile(dbFile, contents, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve(item);
                }
            });
        });
    }
};